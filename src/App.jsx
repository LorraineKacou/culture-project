import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Contact from './components/Contact'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <Contact nom="Kacou"
      prenom="Lorraine"
      online={true}/>
      
      
      <Contact nom="Bob"
      prenom="Marley"
      online={false}/>
      
    </div>
  )
}

export default App
